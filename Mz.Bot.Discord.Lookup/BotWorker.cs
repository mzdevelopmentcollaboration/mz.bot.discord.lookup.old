using System;
using System.Threading;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace Mz.Bot.Discord.Lookup
{
    public class Worker : BackgroundService
    {
        private readonly CommandService _discordCommandService = new();
        private readonly DiscordSocketClient _discordSocketClient = new();
        
        private async Task DiscordSocketClientOnMessageReceived(SocketMessage socketMessage)
        {
            if (socketMessage is not SocketUserMessage message) { return; }
            var commandIndicatorPosition = 0;
            if (!(message.HasCharPrefix('!', ref commandIndicatorPosition) || 
                  message.HasMentionPrefix(_discordSocketClient.CurrentUser, ref commandIndicatorPosition)) || 
                message.Author.IsBot) { return; }
            var commandContext = new SocketCommandContext(_discordSocketClient, message);
            await _discordCommandService.ExecuteAsync(commandContext, commandIndicatorPosition, null);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _discordSocketClient.Ready += DiscordSocketClientOnReady;
                _discordSocketClient.MessageReceived += DiscordSocketClientOnMessageReceived;
                await _discordCommandService.AddModulesAsync(Assembly.GetEntryAssembly(), null);
                await _discordSocketClient.LoginAsync(TokenType.Bot,
                    Environment.GetEnvironmentVariable("discordApplicationSecret"));
                await _discordSocketClient.StartAsync();
                    await Task.Delay(-1, stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await Program.WebBrowser.CloseAsync();
        }

        private async Task DiscordSocketClientOnReady()
        {
            await _discordSocketClient.SetGameAsync("On Standby",
                null, ActivityType.Watching);
        }
    }
}
