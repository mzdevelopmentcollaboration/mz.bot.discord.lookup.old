using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

using PuppeteerSharp;

namespace Mz.Bot.Discord.Lookup
{
    public class Program
    {
        public static Browser WebBrowser;

        public static async Task Main(string[] args)
        {
            await new BrowserFetcher().DownloadAsync();
            WebBrowser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true,
                Args = new[] { "--no-sandbox" }
            });
            await CreateHostBuilder(args).Build().RunAsync();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices(services =>
                {
                    services.AddHostedService<Worker>();
                });
    }
}
