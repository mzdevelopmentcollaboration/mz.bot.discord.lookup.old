﻿using System;
using System.Linq;
using System.Threading.Tasks;

using HtmlAgilityPack;
using PuppeteerSharp;

using Mz.Bot.Discord.Lookup.Models;

namespace Mz.Bot.Discord.Lookup.Manager
{
    internal class WebManager
    {
        public static async Task<Page> ScrapeWebContentAsync(string toBeLookedUpIpAddress,
            Page toBeScrapeWebPage)
        {
            var randomNumberGenerator = new Random();
            await toBeScrapeWebPage.SetRequestInterceptionAsync(true);
            toBeScrapeWebPage.Request += ToBeScrapeWebPageOnRequest;
            await toBeScrapeWebPage.EmulateAsync(Puppeteer.Devices.ElementAt(
                randomNumberGenerator.Next(0, Puppeteer.Devices.Count - 1)).Value);
            await toBeScrapeWebPage.GoToAsync(ProviderInformation.Url + toBeLookedUpIpAddress);
            return toBeScrapeWebPage;
        }

        private static void ToBeScrapeWebPageOnRequest(object sender, RequestEventArgs e)
        {
            if (e.Request.ResourceType == ResourceType.Image ||
                e.Request.ResourceType == ResourceType.Img ||
                e.Request.ResourceType == ResourceType.Media) { e.Request.AbortAsync(); }
            else { e.Request.ContinueAsync(); }
        }

        public static IpInformation ParseWebContent(HtmlDocument webPageHtml, string ipAddress)
        {
            const string missingDataMessage = "No Data was found!";
            var hostname = webPageHtml.DocumentNode.SelectSingleNode(ProviderInformation.HostnameXPath) ??
                HtmlNode.CreateNode(missingDataMessage);
            var isp = webPageHtml.DocumentNode.SelectSingleNode(ProviderInformation.IspXPath) ??
                HtmlNode.CreateNode(missingDataMessage);
            var country = webPageHtml.DocumentNode.SelectSingleNode(ProviderInformation.CountryXPath) ?? 
                HtmlNode.CreateNode(missingDataMessage);
            var city = webPageHtml.DocumentNode.SelectSingleNode(ProviderInformation.CityXPath) ?? 
                HtmlNode.CreateNode(missingDataMessage);
            return new IpInformation
            {
                Isp = isp.InnerText,
                IpAddress = ipAddress,
                City = city.InnerText,
                Country = country.InnerText,
                Hostname = hostname.InnerText
            };
        }
    }
}
