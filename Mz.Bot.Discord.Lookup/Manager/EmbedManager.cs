﻿using Discord;

using Mz.Bot.Discord.Lookup.Models;

namespace Mz.Bot.Discord.Lookup.Manager
{
    internal class EmbedManager
    {
        public static Embed GenerateEmbedCard(IpInformation ipInformation)
        {
            var embedCard = new EmbedBuilder
            {
                Title = $"The following Data has been found for {ipInformation.IpAddress}!",
                Description = "View all available Information via the Link above!",
                Url = ProviderInformation.Url + ipInformation.IpAddress
            };
            embedCard.AddField("Hostname", ipInformation.Hostname, true);
            embedCard.AddField("ISP", ipInformation.Isp, true);
            embedCard.AddField("Country", ipInformation.Country, true);
            embedCard.AddField("City", ipInformation.City, true);
            return embedCard.Build();
        }

        public static Embed GenerateErrorEmbedCard(string errorMessage)
        {
            var errorEmbedCard = new EmbedBuilder
            {
                Title = "An Error occurred during your Request!",
                Description = errorMessage
            };
            return errorEmbedCard.Build();
        }
    }
}
