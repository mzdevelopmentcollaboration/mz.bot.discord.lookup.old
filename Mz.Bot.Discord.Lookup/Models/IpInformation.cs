﻿namespace Mz.Bot.Discord.Lookup.Models
{
    internal class IpInformation
    {
        private string _isp = "No ISP was found!";
        public string Isp
        {
            get => _isp;
            set
            {
                if (value != string.Empty) { _isp = value; }
            }
        }
        private string _city = "No City was found!";
        public string City
        {
            get => _city;
            set
            {
                if (value != string.Empty) { _city = value; }
            }
        }
        private string _country = "No Country was found!";
        public string Country
        {
            get => _country;
            set
            {
                if (value != string.Empty) { _country = value; }
            }
        }
        private string _hostname = "No Hostname was found!";
        public string Hostname
        {
            get => _hostname;
            set
            {
                if (value != string.Empty) { _hostname = value; }
            }
        }
        private string _ipAddress = "No IP was found!";
        public string IpAddress
        {
            get => _ipAddress;
            set
            {
                if (value != string.Empty) { _ipAddress = value; }
            }
        }
    }
}
