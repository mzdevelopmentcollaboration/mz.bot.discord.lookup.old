﻿using System.Net;
using System.Threading.Tasks;

using HtmlAgilityPack;
using Discord.Commands;

using Mz.Bot.Discord.Lookup.Manager;

namespace Mz.Bot.Discord.Lookup.Modules
{
    [Group("ip")]
    public class Lookup : ModuleBase<SocketCommandContext>
    {
        [Command("lookup")]
        public async Task LookupIpAddress([Remainder] string toBeLookedUpIpAddress)
        {
            if (toBeLookedUpIpAddress == null ||
                !toBeLookedUpIpAddress.Contains('.') ||
                !IPAddress.TryParse(toBeLookedUpIpAddress, out _))
            {
                await ReplyAsync(embed: EmbedManager
                    .GenerateErrorEmbedCard("An invalid IP was provided!"));
                return;
            }
            var scrapedWebPage = await WebManager
                .ScrapeWebContentAsync(toBeLookedUpIpAddress,
                    await Program.WebBrowser.NewPageAsync());
            var webPageContentHtml = new HtmlDocument();
            webPageContentHtml.LoadHtml(await scrapedWebPage.GetContentAsync());
            await ReplyAsync(embed: EmbedManager.GenerateEmbedCard(WebManager
                    .ParseWebContent(webPageContentHtml, 
                        toBeLookedUpIpAddress)));
            await scrapedWebPage.CloseAsync();
            await scrapedWebPage.DisposeAsync();
        }
    }
}
