﻿namespace Mz.Bot.Discord.Lookup
{
    internal class ProviderInformation
    {
        public static string Url = "https://whatismyipaddress.com/ip/";
        public static string HostnameXPath = "/html/body/div/div/div/div/div/article/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div/div[1]/p[2]/span[2]";
        public static string IspXPath = "/html/body/div/div/div/div/div/article/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div/div[1]/p[4]/span[2]";
        public static string CountryXPath = "/html/body/div/div/div/div/div/article/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div/div[1]/p[10]/span[2]";
        public static string CityXPath = "/html/body/div/div/div/div/div/article/div/div/div[1]/div/div[2]/div/div[1]/div/div[2]/div/div/div/div/div[1]/p[12]/span[2]";
    }
}
