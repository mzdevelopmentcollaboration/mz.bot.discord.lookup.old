#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/runtime:5.0 AS base
WORKDIR /app

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["Mz.Bot.Discord.Lookup/Mz.Bot.Discord.Lookup.csproj", "Mz.Bot.Discord.Lookup/"]
RUN dotnet restore "Mz.Bot.Discord.Lookup/Mz.Bot.Discord.Lookup.csproj"
COPY . .
WORKDIR "/src/Mz.Bot.Discord.Lookup"
RUN dotnet build "Mz.Bot.Discord.Lookup.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Mz.Bot.Discord.Lookup.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .

RUN apt-get update \
    && apt-get install -y wget gnupg \
    && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
    && apt-get update \
    && apt-get install -y google-chrome-stable fonts-ipafont-gothic fonts-wqy-zenhei fonts-thai-tlwg fonts-kacst fonts-freefont-ttf libxss1 \
        --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["dotnet", "Mz.Bot.Discord.Lookup.dll"]
