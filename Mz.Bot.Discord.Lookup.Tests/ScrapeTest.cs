using Xunit;
using System;
using System.Linq;
using System.Threading.Tasks;

using PuppeteerSharp;

// ReSharper disable UnusedVariable

namespace Mz.Bot.Discord.Lookup.Tests
{
    public class ScrapeTest
    {
        [Fact]
        public async Task ScrapeWebPageHeadlessAsync()
        {
            await new BrowserFetcher().DownloadAsync();
            var webBrowser = await Puppeteer.LaunchAsync(new LaunchOptions { Headless = true });
            var randomNumberGenerator = new Random();
            await using var webPage = await webBrowser.NewPageAsync();
            await webPage.EmulateAsync(Puppeteer.Devices.ElementAt(
                randomNumberGenerator.Next(0, Puppeteer.Devices.Count - 1)).Value);
            await webPage.GoToAsync($"https://whatismyipaddress.com/ip/9.9.9.9");
            var webPageContent = await webPage.GetContentAsync();
            await webPage.CloseAsync();
            await webBrowser.CloseAsync();
        }

        [Fact]
        public void GetEmulationDevices()
        {
            var randomNumberGenerator = new Random();
            var randomIndex = randomNumberGenerator.Next(0, Puppeteer.Devices.Count - 1);
            var selectedDevice = Puppeteer.Devices.ElementAt(randomIndex);
        }
    }
}
